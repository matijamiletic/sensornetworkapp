from sensornetwork.Sensors.SensorBase import SensorBase
from sensornetwork.Communication.ClientUdp import ClientUdp
from sensornetwork.Communication.DataSender import DataSender
from sensornetwork.Configuration.Configuration import Configuration
import socket
import time
import json
import sys
#import time
#from datetime import datetime

#defaultna vrijednost host i port parametra ako se ne zadaju prilikom pokretanja skripte
#klijentska aplikacija salje podatke na server koji slusa na tom host i portu
host, port = "localhost", 9996
if len(sys.argv) == 3:
    host, port = sys.argv[1], int(sys.argv[2])
elif len(sys.argv) == 2:
    host = sys.argv[1]
    port = port
#otvaranje socketa preko kojeg ce se razmjenivati podaci
socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#data = {'timestamp': 1496650817.09, 'sensorType': 'dht22', 'deviceId': 'RPI1', 'sensorId': 13, 'sensorParameters': 1, 'data': {'temperature': 12.54, 'humidity': 0.45}};

#data = {"type": "notification check", "condition" : "check"}

#data = {"type": "sensorOnOff", 'datastreamId':'6', 'sensorOn':'False'}

data = {'type':'change conditions', 'datastreamId': 6, 'condition':[1,3]}

#pokretanje klijentske apikacija 
clientUdp = ClientUdp(host, port);
clientUdp.sendData(data, 0)

