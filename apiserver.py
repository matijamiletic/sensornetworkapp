from bottle import Bottle, route, run, template, get, post, put, response, request, static_file
from sensornetwork.DataStorage.DataStorageBase import DataStorageBase
from sensornetwork.Server.SensorServer import SensorServer
import json

with open("databaseConnection.js") as dataFile:    
    dataFile = json.load(dataFile)

hostname = dataFile["hostname"]
username = dataFile["username"]
password = dataFile["password"]
databaseName = dataFile["dbName"]
storageType = dataFile["storageType"] 

storageParameters = { "hostname":hostname, "user":username, "password":password, "dbName":databaseName}
notificationConditions = [] 
sensorServer = SensorServer(storageType, storageParameters, notificationConditions)

sensorDefinitionDict = {
    "dht22" : 
    {
        "sensorClass" : "SensorDht22",
        "dataKeys" : ["temperature", "humidity"]
    },
    "slm393" : 
    {
        "sensorClass" : "SensorSlm393",
        "dataKeys" : ["rad"]
    },
    "stcs230" : 
    {
        "sensorClass" : "SensorStcs230",
        "dataKeys" : ["R", "G", "B"]
    },
    "hcsr501" : 
    {
        "sensorClass" : "SensorHcsr501",
        "dataKeys" : ["movement"]
    },
    "hcsr04" : 
    {
        "sensorClass" : "SensorHcsr04",
        "dataKeys" : ["distance"]
    },
    "rpi":
    {
        "sensorClass" : "SensorRpi",
        "dataKeys" : ["cpuTemperature"]
    }
}

app = Bottle()

@app.get('/frontend/<filename>')
def serverStatic(filename):
    return static_file(filename, root='frontend')

@app.get("/sensordefinition")
@app.get("/sensordefinition/")
@app.get("/sensordefinition/<sensorType>")
def sensorDefinition(sensorType = "all"):
        response.content_type = "application/json; charset=utf8"

        if(sensorType in sensorDefinitionDict):
            return json.dumps(sensorDefinitionDict[sensorType])

        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
        response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'
        
        return json.dumps(sensorDefinitionDict)

@app.get("/sensortypes")
@app.get("/sensortypes/")
@app.get("/sensortypes/<sensorId>")
def sensorTypes(sensorId = "all"):
        response.content_type = "application/json; charset=utf8"

        if(sensorId in sensorServer.sensorTypes):
            return json.dumps(sensorServer.sensorTypes[sensorId])

        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
        response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'
    
        return json.dumps(sensorServer.sensorTypes)

@app.get("/sensordata")
def sensorDataGet():
        data = request.query
        conditions = dict()

        if(data.get('deviceId')): #string
            conditions.update({'deviceId':data['deviceId']})
        if(data.get('sensorId')): #string
            conditions.update({'sensorId':data['sensorId']}) 
        if(data.get('timeFrom')): #float (ili double)
            conditions.update({'timeFrom':data['timeFrom']}) 
        if(data.get('timeTo')): #float
            conditions.update({'timeTo':data['timeTo']}) 
        if(data.get('sensorType')): #string
            conditions.update({'sensorType':data['sensorType']}) 
        if(data.get('offset')): #intiger
            conditions.update({'offset':data['offset']})
        if(data.get('limit')): #intiger
            conditions.update({'limit':data['limit']})

        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
        response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'   
        result = sensorServer.dataStorage.read(conditions)

        return json.dumps(result)

@app.put("/sensordata") #put stavlja podatke u bazu
def sensorDataPut():
        result = sensorServer.handleData(json.dumps(request.json))
        return result

@app.get("/checkconfig") #na serveru se uvijek prvo napravi get, zato je potrebno imati dvije rute
@app.post("/checkconfig")
def checkConfig():
        errorMessages = []
        warningMessages = []
        validationOk = False

        if(request.method == 'POST'): #ako bi dohvacali preko browsera result
            request.content_type = "application/json; charset=utf8"
            configData=json.loads(request.json)

            if("deviceId" in configData):
                if(not isinstance(configData['deviceId'], basestring)):
                    errorMessages.append("Argument deviceId must be a string")
            else:
                errorMessages.append("Missing 'deviceId' argument")

            if("sensors" in configData): #ako polje sensors nije prazno
                if(isinstance(configData['sensors'], list)):
                    if(configData['sensors']):
                        for sensor in configData['sensors']:
                            if(not "sensorParameters" in sensor): 
                                warningMessages.append("Elememt 'sensorParameters' for sensorId=" + sensor['sensorId'] + " doesn't exist")
                            elif(not "pins" in sensor['sensorParameters']):
                                warningMessages.append("Elememt 'pins' for sensorId=" + sensor['sensorId'] + " doesn't exist")
                            elif(not isinstance(sensor, dict)): errorMessages.append("Sensor's elements must be a dictionary")
                    else:
                        errorMessages.append("There is no sensor data")
                else:
                    errorMessages.append("Array element 'sensor' must be a list")
            else:
                errorMessages.append("Missing 'sensors' argument")
    
            if(not errorMessages): 
                errorMessages = ["none"]
                validationOk = True 
            if(not warningMessages):  
                warningMessages = ["none"] 
                validationOk = True

        print json.dumps(configData)
        result = {"validationOk": validationOk, "errors": ', '.join(errorMessages), "warnings": ', '.join(warningMessages)}
        return result

try:
    run (app, host = "localhost", port = 8080)
except:
    pass