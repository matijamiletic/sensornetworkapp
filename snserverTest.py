from sensornetwork.Communication.ServerUdp import ServerUdp
from sensornetwork.Server.TestServer import TestServer
from sensornetwork.Configuration.Configuration import Configuration
import SocketServer
import json
import sys

#polje s uvjetima za slanje notifikacija, ako je uvjet iz polja zadovoljen primljeni podaci salju se na mail
notificationConditions = Configuration.loadConfiguration('notificationConditions.json')

#defaultna vrijednost host i port parametra ako se ne zadaju prilikom pokretanja skripte
host, port = "192.168.43.191", 9998
if len(sys.argv) == 3:
    host, port = sys.argv[1], int(sys.argv[2])
elif len(sys.argv) == 2:
    host = sys.argv[1]
    port = port

#stvaranje senor server objekta koji sluzi za obradu podataka i pokretanje UDP servera
ServerUdp.sensorServer = TestServer()
#nakon stvaranja objekta sensorServer moze se pozvati funkcija koja omogucuje konekciju na bazu
#korisnik bira hoce li se podaci slati na mysql bazu ili ne, ovisno o njegovom inputu
input = raw_input("Connect to database?Y/N ")
if (input == 'Y' or input == 'y'):
    ServerUdp.sensorServer.connectToDatabase()
#pokretanje servera koji radi na zadanom hostu i portu i prima podatke s klijentske aplikacije
server = SocketServer.UDPServer((host, port), ServerUdp)
#ispis kako bi korisnik znao da je server ON nakon pokretanja ili ne pokretanja spajanja na bazu
print "Server is running"
#server je upaljen dok ga prislino ne ugasimo ili dok ne dode do pogreske
server.serve_forever()