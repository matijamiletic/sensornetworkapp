import socket
import sys, thread
import json
import random
import types
from datetime import datetime
from Getch import *

host, port = "localhost", 9998
if len(sys.argv) == 3:
    host, port = sys.argv[1], int(sys.argv[2])

WORKING_MODE_SINGLE = 1
WORKING_MODE_CONTINUOUS = 2
workingMode = WORKING_MODE_CONTINUOUS
sendDelay = 0.5

configurationArray = [
    {
        "sensorType": "dht22",
        "description": "senzor temperature i vlage",
        "data":
        {
            "temperature": [-40.0, 60.0],
            "humidity": [0.00, 1.00]
        }
    },
    {
        "sensorType": "slm393",
        "description": "senzor svjetlosti",
        "data":
        {
            "rad": [True, False],
        }
    },
    {
        "sensorType": "stcs230",
        "description": "senzor za prepoznavanje boje",
        "data":
        {
            "R": [0, 255],
            "G": [0, 255],
            "B": [0, 255]
        }
    },
    {
        "sensorType": "hcsr501",
        "description": "senzor za otkrivanje pokreta",
        "data":
        {
            "movement" : [True, False]
        }
    },
    {
        "sensorType": "hcsr04",
        "description": "senzor za mjerenje udaljenosti",
        "data":
        {
            "distance" : [2, 400]
        }
    }
]

activeSensors = []
for i in range(0, len(configurationArray)):
    activeSensors.append(False)

keyPressPressedKey = ""
keyPressIsNewKey = False

pressedKey = ""
sendData = False
outputData = False
sendingPaused = False

def printMenu():
    print "\nAvailable options:\n"
    print " w - toggle working mode"
    print " o - pause/resume output"
    print " s - pause/resume sending"
    print " i - Info & Menu"
    print " q - quit"

    print "\nAvailable sensors:\n"
    for index, sensor in enumerate(configurationArray):
        print " " + str(index) + " - " + sensor["sensorType"] + " - " + sensor["description"]

    print

def printWorkingMode(workingMode):
    if workingMode == WORKING_MODE_CONTINUOUS:
        print "Working mode: CONTINUOUS -", workingMode
    else:
        print "Working mode: SINGLE -", workingMode

def printDataOutputNote():
    global workingMode, outputData, WORKING_MODE_CONTINUOUS

    if workingMode == WORKING_MODE_CONTINUOUS and isSendingData() and not outputData:
        print "\nNOTE: Data is still being sent, but output is paused. Will be resumed when a sensor is choosen or data output resumed."

def printActiveSensors():
    global activeSensors

    sensorsString = ""
    for i in range(0, len(activeSensors)):
        if activeSensors[i]:
            sensorsString = sensorsString + " " + str(i)

    print "Active sensors:", sensorsString


def printInfo():
    global outputData, sendingPaused

    outputData = False

    print "\n"

    printMenu()
    print "sendingPaused:", sendingPaused
    print "sendDelay:", sendDelay
    printWorkingMode(workingMode)
    print
    printActiveSensors()
    printDataOutputNote()

def keyPress(exitChar="q"):
    global keyPressPressedKey, keyPressIsNewKey

    while keyPressPressedKey != exitChar:
        keyPressPressedKey = getch().lower()
        keyPressIsNewKey = True

def isInt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

def isSendingData():
    global sendData, sendingPaused, activeSensors

    sensorActive = False
    for i in range(0, len(activeSensors)):
        if activeSensors[i]:
            sensorActive = True
            break

    return sendData and not sendingPaused and sensorActive

def sendSensorData(sensorIndex):
    global configurationArray, outputData

    randomData = {
                    "deviceId": random.randint(1,5), 
                    "sensorId": random.randint(1,5),
                    "sensorType" : configurationArray[sensorIndex]["sensorType"], 
                    "timestamp" : (datetime.utcnow() - datetime(1970, 1, 1)).total_seconds(),
                    "data" : {}
                 }
    
    for dataKey in configurationArray[sensorIndex]["data"]:
        if type(configurationArray[sensorIndex]["data"][dataKey][0]) == types.BooleanType and type(configurationArray[sensorIndex]["data"][dataKey][1]) == types.BooleanType : 
            randomData["data"].update( { dataKey : bool(random.getrandbits(1)) } )
        elif isinstance(configurationArray[sensorIndex]["data"][dataKey][0], int) and isinstance(configurationArray[sensorIndex]["data"][dataKey][1], int):
            randomData["data"].update({dataKey : random.randint(configurationArray[sensorIndex]["data"][dataKey][0], configurationArray[sensorIndex]["data"][dataKey][1])})
        elif isinstance(configurationArray[sensorIndex]["data"][dataKey][0], float) and isinstance(configurationArray[sensorIndex]["data"][dataKey][1], float):
            randomData["data"].update({dataKey : float(format(random.uniform(configurationArray[sensorIndex]["data"][dataKey][0], configurationArray[sensorIndex]["data"][dataKey][1]), '.2f'))})
            
    randomGeneratedData = json.dumps(randomData)
    if outputData:
        print "Sending data:", randomGeneratedData

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.sendto(randomGeneratedData, (host, port))
    received = sock.recv(1024)

    if outputData:
        print "Received from server: {}".format(received)

thread.start_new_thread(keyPress, ())

printInfo()

try:
    while True:

        pressedKey = keyPressPressedKey.lower()

        if keyPressIsNewKey:
            if keyPressPressedKey == "w":
                if workingMode == WORKING_MODE_CONTINUOUS:
                    workingMode = WORKING_MODE_SINGLE
                else:
                    workingMode = WORKING_MODE_CONTINUOUS
                printWorkingMode(workingMode)

            elif keyPressPressedKey == "i":

                printInfo()

            elif keyPressPressedKey == "o":
                outputData = not outputData
                if outputData:
                    print "RESUMING DATA OUTPUT IN 1 second..."
                    time.sleep(1)
                else:
                    print "\nDATA OUTPUT PAUSED"
                    printDataOutputNote()

            elif keyPressPressedKey == "s":
                sendingPaused = not sendingPaused

                if sendingPaused:
                    print "Sending paused"
                else:
                    print "Sending resumed"
                    printDataOutputNote()

            elif keyPressPressedKey == "+":
                sendDelay += 0.125
                print "Send delay is now:", sendDelay

            elif keyPressPressedKey == "-":
                sendDelay -= 0.125
                if sendDelay < 0:
                    sendDelay = 0.
                print "Send delay is now:", sendDelay

            elif keyPressPressedKey == "q":
                print "QUIT"
                break

            elif isInt(keyPressPressedKey):
                outputData = False

                sensorIndex = int(keyPressPressedKey)

                if (sensorIndex < len(activeSensors) and (sensorIndex >= 0)):
                    activeSensors[sensorIndex] = not activeSensors[sensorIndex]
                    print "Sensor", sensorIndex, "active:", activeSensors[sensorIndex]
                else:
                    print "Sensor not available:", sensorIndex

                sendData = True

                printActiveSensors()

                if isSendingData():
                    if workingMode == WORKING_MODE_CONTINUOUS:
                        print "RESUMING DATA OUTPUT IN 1 second..."
                        time.sleep(1)
                else:
                    print "*** DATA IS NOT BEING SENT ***"

                outputData = True

            keyPressIsNewKey = False

        randomData = []

        if sendData and not sendingPaused:
            for i in range(0, len(activeSensors)):
                if activeSensors[i]:
                    sendSensorData(i)

            if workingMode == WORKING_MODE_SINGLE:
                sendData = False


        if workingMode == WORKING_MODE_CONTINUOUS:
            sendData = True

        time.sleep(sendDelay)

except KeyboardInterrupt: 
    pass    

